#ifndef PARAMETRE_H
#define PARAMETRE_H

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>
#include <QMessageBox>
#include <QSqlDatabase>
#include "dbmanager.h"
#include "routeur.h"


class Routeur;

class Parametre
{
public:
   // constructeurs
   Parametre();
   Parametre(int, QString, QString, QString, int);
   Parametre(QString, QString, QString, int);

   //QList<Parametre> getAll();

   // getters
   int getId();
   QString getParefeu();
   QString getFiltrage();
   QString getControl();
   int getRouteurid();
   Routeur getRouteur();


   // setters
   void setId(int);
   void setParefeu(QString);
   void setFiltrage(QString);
   void setControl(QString);
   void setRouteurid(int);

   QList<Parametre> getAllByRouteur(int) const;

   // interactions avec la base de données
   // en admetant que nous disposons de l'objet:
   bool save() const; // enregistrer l'objet dans la base
   bool update() const; // modifier l'objet dans la base

   bool remove(); // le supprimer
   bool removeAllBelongToRouteur(int&); //supprimer tous les objet (pas disponible au niveau de la vue)

   QList<Parametre> getAll() const; // recuperer la liste des router
   Parametre getOne(const int& id) const;



private:
   QSqlDatabase m_db;

   int id;
   QString parefeu;
   QString filtrage;
   QString control;
   int routeurid;


};

#endif // PARAMETRE_H
