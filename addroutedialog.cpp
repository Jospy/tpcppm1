#include "addroutedialog.h"
#include "ui_addroutedialog.h"

AddRouteDialog::AddRouteDialog(int routeurid, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddRouteDialog)
{
    this->routeurid = routeurid;
    ui->setupUi(this);
}

AddRouteDialog::~AddRouteDialog()
{
    delete ui;
}

void AddRouteDialog::on_cancelBtn_clicked()
{
    this->close();
}

void AddRouteDialog::on_enregistrerRouteBtn_clicked()
{
    QString ip = ui->ipInput->text();
    QString mask = ui->maskInput->text();
    QString passerelle = ui->passerelleInput->text();

    qDebug() << ip << mask << passerelle << this->routeurid;
    Route r(ip, mask, passerelle, this->routeurid);
    if(r.save()){
      this->close();
    };
}
