#ifndef INTERFACE_H
#define INTERFACE_H

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>
#include <QMessageBox>
#include <QSqlDatabase>
#include "dbmanager.h"
#include "routeur.h"


class Routeur;

class Interface
{
public:
   // constructeurs
   Interface();
   Interface(int, QString, QString, int, int);
   Interface(QString, QString, int, int);

   //QList<Interface> getAll();

   // getters
   int getId();
   QString getIp();
   QString getNom();
   int getConnection();
   int getRouteurid();
   Routeur getRouteur();

   // setters
   void setId(int);
   void setIp(QString);
   void setNom(QString);
   void setConnection(int);
   void setRouteurid(int);

   QList<Interface> getAllByRouteur(int) const;

   // interactions avec la base de données
   // en admetant que nous disposons de l'objet:
   bool save() const; // enregistrer l'objet dans la base
   bool update() const; // modifier l'objet dans la base

   bool remove(); // le supprimer
   bool removeAllBelongToRouteur(int&); //supprimer tous les objet (pas disponible au niveau de la vue)

   QList<Interface> getAll() const; // recuperer la liste des router
   Interface getOne(const int& id) const;



private:
   QSqlDatabase m_db;

   int id;
   QString ip;
   QString nom;
   int connection;
   int routeurid;

};

#endif // INTERFACE_H
