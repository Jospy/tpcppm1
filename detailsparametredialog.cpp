#include "detailsparametredialog.h"
#include "ui_detailsparametredialog.h"

DetailsParametreDialog::DetailsParametreDialog(int id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DetailsParametreDialog)
{
    ui->setupUi(this);
    Parametre p = p.getOne(id);
    this->parametreid = p.getId();
    ui->parefeuInput->setText(p.getParefeu());
    ui->filtrageInput->setText(p.getFiltrage());
    ui->controlInput->setText(p.getControl());
}

DetailsParametreDialog::~DetailsParametreDialog()
{
    delete ui;
}


void DetailsParametreDialog::on_removeBtn_clicked()
{
    QMessageBox::StandardButton resBtn;
    resBtn = QMessageBox::question( this, "Suppression du parametre",tr("Etes vous sur de vouloir supprimer ce parametre?"),
    QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
    QMessageBox::Yes);
    if (resBtn == QMessageBox::Yes)
    {
        Parametre p = p.getOne(this->parametreid);
        if(p.remove()){
          this->close();
        };
    }
}

void DetailsParametreDialog::on_cancelBtn_clicked()
{
    this->close();
}

void DetailsParametreDialog::on_enregistrerBtn_clicked()
{
    QString parefeu = ui->parefeuInput->text();
    QString filtrage = ui->filtrageInput->text();
    QString control = ui->controlInput->text();

    Parametre p = p.getOne(this->parametreid);
    p.setParefeu(parefeu);
    p.setFiltrage(filtrage);
    p.setControl(control);
    if(p.update())
    {
        this->close();
    }
}
