#ifndef ADDROUTEURDIALOG_H
#define ADDROUTEURDIALOG_H

#include <QDialog>
#include "routeur.h"

namespace Ui {
class AddRouteurDialog;
}

class AddRouteurDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddRouteurDialog(QWidget *parent = nullptr);
    ~AddRouteurDialog();

    void on_saveBtn_clicked();

private slots:
    void on_cancelBtn_clicked();

    void on_enregistrerBtn_clicked();

private:
    Ui::AddRouteurDialog *ui;
};

#endif // ADDROUTEURDIALOG_H
