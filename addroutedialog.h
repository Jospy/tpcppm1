#ifndef ADDROUTEDIALOG_H
#define ADDROUTEDIALOG_H

#include <QDialog>
#include "route.h"
namespace Ui {
class AddRouteDialog;
}

class AddRouteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddRouteDialog(int routeurid, QWidget *parent = nullptr);
    ~AddRouteDialog();

private slots:
    void on_cancelBtn_clicked();

    void on_enregistrerRouteBtn_clicked();

private:
    Ui::AddRouteDialog *ui;
    int routeurid;
};

#endif // ADDROUTEDIALOG_H
