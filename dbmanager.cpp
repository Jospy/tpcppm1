#include "dbmanager.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>

DbManager::DbManager (const QString &path)
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(path);

    if (!m_db.open())
    {
        qDebug() << "Error: connection échouée";
    }
    else
    {
        qDebug() << "Database: connection ok";
    }
}

DbManager::~DbManager()
{
    if (m_db.isOpen())
    {
        m_db.close();
    }
}

bool DbManager::isOpen() const
{
    return m_db.isOpen();
}


bool DbManager::createTable()
{
    qDebug() << "=========== CREATION DES TABLES ET INSERSTION DES DONNEES";
    bool success = false;

    QSqlQuery query;

//    query.prepare("DROP TABLE routeurs;");
//    if (!query.exec())
//    {
//        qDebug() << "Impossible de supprimer la table routeurs.";
//        success = false;
//    }

//    query.prepare("DROP TABLE interfaces;");
//    if (!query.exec())
//    {
//        qDebug() << "Impossible de supprimer la table interfaces.";
//        success = false;
//    }

//    query.prepare("DROP TABLE parametres;");
//    if (!query.exec())
//    {
//        qDebug() << "Impossible de supprimer la table parametres.";
//        success = false;
//    }

//    query.prepare("DROP TABLE routes;");
//    if (!query.exec())
//    {
//        qDebug() << "Impossible de supprimer la table routes.";
//        success = false;
//    }

    query.prepare("CREATE TABLE IF NOT EXISTS `routeurs` ( `id`	INTEGER NOT NULL, `numero`	varchar(100) NOT NULL, `ip`	varchar(100) NOT NULL, PRIMARY KEY(`id` AUTOINCREMENT));");
    if (!query.exec())
    {
        qDebug() << "Impossible de créer la table: 'routeurs' car elle existe déjà.";
        success = false;
    }

    query.prepare("CREATE TABLE IF NOT EXISTS `interfaces` ( `id`	INTEGER NOT NULL, `nom` varchar(100) NOT NULL, `ip` varchar(100) NOT NULL, `connexion` tinyint(1) NOT NULL DEFAULT '0', `routeurid` int(11) NOT NULL, PRIMARY KEY(`id` AUTOINCREMENT), FOREIGN KEY (routeurid) REFERENCES routeurs(id));");
    if (!query.exec())
    {
        qDebug() << "Impossible de créer la table: 'interfaces' car elle existe déjà.";
        success = false;
    }

    query.prepare("CREATE TABLE IF NOT EXISTS `parametres` ( `id`	INTEGER NOT NULL, `parefeu` varchar(100) NOT NULL, `filtrage` varchar(100) NOT NULL, `control` varchar(100) NOT NULL, `routeurid` int(11) NOT NULL, PRIMARY KEY(`id` AUTOINCREMENT), FOREIGN KEY (routeurid) REFERENCES routeurs(id) );");
    if (!query.exec())
    {
        qDebug() << "Impossible de créer la table: 'parametres' car elle existe déjà.";
        success = false;
    }

    query.prepare("CREATE TABLE IF NOT EXISTS `routes` ( `id`	INTEGER NOT NULL, `ip` varchar(100) NOT NULL, `mask` varchar(100) NOT NULL, `passerelle` varchar(100) NOT NULL, `routeurid` int(11) NOT NULL, PRIMARY KEY(`id` AUTOINCREMENT), FOREIGN KEY (routeurid) REFERENCES routeurs(id) );");
    if (!query.exec())
    {
        qDebug() << "Impossible de créer la table: 'routes' car elle existe déjà.";
        success = false;
    }

    query.prepare("INSERT INTO `routeurs` (`id`, `numero`, `ip`) VALUES  ('1', 'r1', '192.168.1.1'), ('2', 'r2', '192.168.2.1'), ('3', 'r3', '192.168.3.1');");
    if (!query.exec())
    {
        qDebug() << "La table: 'routeurs' contient deja les donnees";
        success = true;
    }

    query.prepare("INSERT INTO `routes` (`id`, `ip`, `mask`, `passerelle`, `routeurid`) VALUES ('1', '192.168.1.1', '24', '192.168.1.1', '1'), ('2', '192.168.1.2', '24', '192.168.1.1', '2'), ('3', '192.168.1.1', '24', '192.168.1.1', '3'), ('4', '192.168.1.1', '24', '192.168.1.1', '1'), ('5', '192.168.1.1', '24', '192.168.1.1', '2'), ('6', '192.168.1.1', '24', '192.168.1.1', '3'), ('7', '192.168.1.1', '24', '192.168.1.1', '1'), ('8', '192.168.1.1', '24', '192.168.1.1', '2');");
    if (!query.exec())
    {
        qDebug() << "La table: 'routes' contient deja les donnees";
        success = true;
    }

    query.prepare("INSERT INTO `interfaces` (`id`, `nom`, `ip`, `connexion`, `routeurid`) VALUES ('1', 'interface1', '192.168.1.1', '0', '1'), ('2', 'interface2', '192.168.1.2', '1', '2'), ('3', 'interface3', '192.168.1.1', '0', '3'), ('4', 'interface4', '192.168.1.1', '1', '1'), ('5', 'interface5', '192.168.1.1', '0', '2'), ('6', 'interface6', '192.168.1.1', '1', '3'), ('7', 'interface7', '192.168.1.1', '0', '1'), ('8', 'interface8', '192.168.1.1', '0', '2');");
    if (!query.exec())
    {
        qDebug() << "La table: 'interfaces' contient deja les donnees";
        success = true;
    }

    query.prepare("INSERT INTO `parametres` (`id`, `parefeu`, `filtrage`, `control`, `routeurid`) VALUES ('1', 'parefeu1', 'filtrage1', 'control1',  '1'), ('2', 'parefeu2', 'filtrage2', 'control2',  '2'), ('3', 'parefeu3', 'filtrage3', 'control3',  '3'), ('4', 'parefeu4', 'filtrage4', 'control4',  '1'), ('5', 'parefeu5', 'filtrage5', 'control5',  '2'), ('6', 'parefeu6', 'filtrage6', 'control6',  '3'), ('7', 'parefeu7', 'filtrage7', 'control7',  '1'), ('8', 'parefeu8', 'filtrage8', 'control8',  '2');");
    if (!query.exec())
    {
        qDebug() << "La table: 'parametres' contient deja les donnees";
        success = true;
    }
    qDebug() << "=========== BASE DE DONNEES PRETE";

    return success;
}
