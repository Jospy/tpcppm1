#include "detailsrouteurdialog.h"
#include "detailsroutedialog.h"
#include "detailsinterfacedialog.h"
#include "detailsparametredialog.h"
#include "ui_detailsrouteurdialog.h"
#include "routeur.h"
#include "route.h"

#include <QDebug>

DetailsRouteurDialog::DetailsRouteurDialog(int id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DetailsRouteurDialog)
{
    ui->setupUi(this);
    this->routeur_id = id;
    loadView(id);
}

void DetailsRouteurDialog::loadView(int id) {
    Routeur r = r.getOne(id);
    ui->ipInput->setText(r.getIp());
    ui->numeroInput->setText(r.getNumero());
    reloadRouteTable();
    reloadInterfaceTable();
    reloadParametreTable();
}

void DetailsRouteurDialog::reloadTable()
{

}

DetailsRouteurDialog::~DetailsRouteurDialog()
{
    delete ui;
}


void DetailsRouteurDialog::on_closeDetails_clicked()
{
    this->close();
}

void DetailsRouteurDialog::on_deleteRouteur_clicked()
{
    QMessageBox::StandardButton resBtn;
    resBtn = QMessageBox::question( this, "Suppression du routeur", tr("Etes vous sur de vouloir supprimer ce routeur?"),
    QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
    QMessageBox::Yes);
    if (resBtn == QMessageBox::Yes)
    {
        Routeur r = r.getOne(this->routeur_id);
        if(r.remove()){
          this->close();
        };
    }
}

void DetailsRouteurDialog::on_enregistrerInput_clicked()
{
    QString numero = ui->numeroInput->text();
    QString ip = ui->ipInput->text();

    Routeur r = r.getOne(this->routeur_id);
    r.setIp(ip);
    r.setNumero(numero);
    r.update();
}

void DetailsRouteurDialog::on_addRouteBtn_clicked()
{
    AddRouteDialog a(this->routeur_id);
    a.exec();
}

void DetailsRouteurDialog::on_addParametreBtn_clicked()
{
    AddParametreDialog a(this->routeur_id);
    a.exec();
}

void DetailsRouteurDialog::on_addInterfaceBtn_clicked()
{
    AddInterfaceDialog a(this->routeur_id);
    a.exec();
}


void DetailsRouteurDialog::reloadRouteTable()
{
    ui->routeTableWidget->setRowCount(0);
    Route r;
    QList<Route> l = r.getAllByRouteur(this->routeur_id);
    int taille = l.size();

    for (int row=0; row<taille; row++ )
    {
        int idint = l[row].getId();
        QString idr = QString::number(idint);
        ui->routeTableWidget->insertRow(ui->routeTableWidget->rowCount());
        ui->routeTableWidget->setItem(ui->routeTableWidget->rowCount()-1,0, new QTableWidgetItem(idr));
        ui->routeTableWidget->setItem(ui->routeTableWidget->rowCount()-1,1, new QTableWidgetItem(l[row].getIp()));
        ui->routeTableWidget->setItem(ui->routeTableWidget->rowCount()-1,2, new QTableWidgetItem(l[row].getMask()));
        ui->routeTableWidget->setItem(ui->routeTableWidget->rowCount()-1,3, new QTableWidgetItem(l[row].getPasserelle()));
    }
}


void DetailsRouteurDialog::reloadInterfaceTable()
{
    ui->interfaceTableWidget->setRowCount(0);
    Interface r;
    QList<Interface> l = r.getAllByRouteur(this->routeur_id);
    int taille = l.size();

    for (int row=0; row<taille; row++ )
    {
        int idint = l[row].getId();
        QString idr = QString::number(idint);
        ui->interfaceTableWidget->insertRow(ui->interfaceTableWidget->rowCount());
        ui->interfaceTableWidget->setItem(ui->interfaceTableWidget->rowCount()-1, 0, new QTableWidgetItem(idr));
        ui->interfaceTableWidget->setItem(ui->interfaceTableWidget->rowCount()-1, 1, new QTableWidgetItem(l[row].getIp()));
        ui->interfaceTableWidget->setItem(ui->interfaceTableWidget->rowCount()-1, 2, new QTableWidgetItem(l[row].getNom()));
        ui->interfaceTableWidget->setItem(ui->interfaceTableWidget->rowCount()-1, 3, new QTableWidgetItem(l[row].getConnection() ? "Connecté" : "Non connecté"));
    }
}


void DetailsRouteurDialog::reloadParametreTable()
{
    ui->parametreTableWidget->setRowCount(0);
    Parametre r;
    QList<Parametre> l = r.getAllByRouteur(this->routeur_id);
    int taille = l.size();

    for (int row=0; row<taille; row++)
    {
        int idint = l[row].getId();
        QString idr = QString::number(idint);
        qDebug() << idr << l[row].getParefeu() << l[row].getFiltrage() << l[row].getControl();
        ui->parametreTableWidget->insertRow(ui->parametreTableWidget->rowCount());
        ui->parametreTableWidget->setItem(ui->parametreTableWidget->rowCount()-1,0, new QTableWidgetItem(idr));
        ui->parametreTableWidget->setItem(ui->parametreTableWidget->rowCount()-1,1, new QTableWidgetItem(l[row].getParefeu()));
        ui->parametreTableWidget->setItem(ui->parametreTableWidget->rowCount()-1,2, new QTableWidgetItem(l[row].getFiltrage()));
        ui->parametreTableWidget->setItem(ui->parametreTableWidget->rowCount()-1,3, new QTableWidgetItem(l[row].getControl()));
    }
}

void DetailsRouteurDialog::on_routeTableWidget_cellDoubleClicked(int row, int column)
{
    QModelIndex index = ui->routeTableWidget->selectionModel()->selectedIndexes()[0];
    int id=index.sibling(index.row(), 0).data().toInt();
    DetailsRouteDialog d(id);
    d.exec();
}

void DetailsRouteurDialog::on_reloadRoute_clicked()
{
    reloadRouteTable();
}

void DetailsRouteurDialog::on_reloadParametre_clicked()
{
    qDebug()  << "parametre";
    reloadParametreTable();
}

void DetailsRouteurDialog::on_reloadInterface_clicked()
{
    reloadInterfaceTable();
}

void DetailsRouteurDialog::on_interfaceTableWidget_cellDoubleClicked(int row, int column)
{
    QModelIndex index = ui->interfaceTableWidget->selectionModel()->selectedIndexes()[0];
    int id=index.sibling(index.row(), 0).data().toInt();
    DetailsInterfaceDialog d(id);
    d.exec();
}

void DetailsRouteurDialog::on_parametreTableWidget_cellDoubleClicked(int row, int column)
{
    QModelIndex index = ui->parametreTableWidget->selectionModel()->selectedIndexes()[0];
    int id=index.sibling(index.row(), 0).data().toInt();
    DetailsParametreDialog d(id);
    d.exec();
}
