#ifndef ROUTE_H
#define ROUTE_H

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>
#include <QMessageBox>
#include <QSqlDatabase>
#include "dbmanager.h"
#include "routeur.h"


class Routeur;

class Route
{
public:
   // constructeurs
   Route();
   Route(int, QString, QString, QString, int);
   Route(QString, QString, QString, int);

   //QList<Route> getAll();

   // getters
   int getId();
   QString getIp();
   QString getMask();
   QString getPasserelle();
   int getRouteurid();
   Routeur getRouteur();


   // setters
   void setId(int);
   void setIp(QString);
   void setMask(QString);
   void setPasserelle(QString);
   void setRouteurid(int);

   QList<Route> getAllByRouteur(int) const;

   // interactions avec la base de données
   // en admetant que nous disposons de l'objet:
   bool save() const; // enregistrer l'objet dans la base
   bool update() const; // modifier l'objet dans la base

   bool remove(); // le supprimer
   bool removeAllBelongToRouteur(int&); //supprimer tous les objet (pas disponible au niveau de la vue)

   QList<Route> getAll() const; // recuperer la liste des router
   Route getOne(const int& id) const;



private:
   QSqlDatabase m_db;

   int id;
   QString ip;
   QString mask;
   QString passerelle;
   int routeurid;


};

#endif // ROUTE_H
