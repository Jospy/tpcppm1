#include "addrouteurdialog.h"
#include "ui_addrouteurdialog.h"

#include <QMessageBox>

AddRouteurDialog::AddRouteurDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddRouteurDialog)
{
    ui->setupUi(this);
}

AddRouteurDialog::~AddRouteurDialog()
{
    delete ui;
}


void AddRouteurDialog::on_cancelBtn_clicked()
{
    this->close();
}

void AddRouteurDialog::on_enregistrerBtn_clicked()
{
    QString numero = ui->numeroInput->text();
    QString ip = ui->ipInput->text();

    Routeur r(numero, ip);
    if(r.save()){
      this->close();
    };
    //reloadTable();
}
