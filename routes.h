#ifndef ROUTES_H
#define ROUTES_H
#include <QSqlDatabase>

#endif // ROUTES_H
//INSERT INTO routes (idroute,adrNet,adrMask,adrPass,numeroID)VALUES (idroute,adrNet,adrMask,adrPass,numeroID);

class RoutesManager
{
public:
   RoutesManager() ;//(const QString& path);

   bool addRoute(const QString& adresseIP) const;

   bool removeRoute(const QString& name);

   bool routeExists(const QString& name) const;

   bool removeAllRoutes();

   QString getAllRoutes() const;

private:
   QSqlDatabase m_db;

};
