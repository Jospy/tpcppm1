#include "addinterfacedialog.h"
#include "ui_addinterfacedialog.h"

AddInterfaceDialog::AddInterfaceDialog(int routeurid, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddInterfaceDialog)
{
    this->routeurid = routeurid;
    ui->setupUi(this);
}

AddInterfaceDialog::~AddInterfaceDialog()
{
    delete ui;
}

void AddInterfaceDialog::on_enregistrerBtn_clicked()
{
    QString ip = ui->ipInput->text();
    QString nom = ui->nomInput->text();
    int connection = ui->connectionInput->isChecked() ? 1 : 0;

    qDebug() << ip << nom << connection << this->routeurid;
    Interface r(ip, nom, connection, this->routeurid);
    if(r.save()){
      this->close();
    };
}

void AddInterfaceDialog::on_cancelBtn_2_clicked()
{
    this->close();
}
