#include "parametre.h"


Parametre::Parametre(){
    this->id = 0;
    this->parefeu = "";
    this->filtrage = "";
    this->control = "";
    this->routeurid = 0;
}

Parametre::Parametre(int id, QString parefeu, QString filtrage, QString control, int routeurid ){
    this->id = id;
    this->parefeu = parefeu;
    this->filtrage = filtrage;
    this->control = control;
    this->routeurid = routeurid;
}

Parametre::Parametre(QString parefeu, QString filtrage, QString control, int routeurid ){
    this->id = 0;
    this->parefeu = parefeu;
    this->filtrage = filtrage;
    this->control = control;
    this->routeurid = routeurid;
}

Parametre Parametre::getOne(const int& id) const
{
    Parametre p;
    QSqlQuery q;
    q.prepare("SELECT * FROM parametres WHERE id = (:id)");
    q.bindValue(":id", id);
    if (q.exec())
    {
        int id = q.record().indexOf("id");
        int parefeu = q.record().indexOf("parefeu");
        int filtrage = q.record().indexOf("filtrage");
        int control = q.record().indexOf("control");
        int routeurid = q.record().indexOf("routeurid");
        if (q.next())
        {
            p.setId(q.value(id).toInt());
            p.setFiltrage(q.value(filtrage).toString());
            p.setControl(q.value(control).toString());
            p.setRouteurid(q.value(routeurid).toInt());
            p.setParefeu(q.value(parefeu).toString());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec: Parametre introuvable"), q.lastError().text());
    }
    return p;
}

QList<Parametre> Parametre::getAll() const
{
    QSqlQuery q("SELECT * FROM parametres");
    int id = q.record().indexOf("id");
    int parefeu = q.record().indexOf("parefeu");
    int filtrage = q.record().indexOf("filtrage");
    int control = q.record().indexOf("control");
    int routeurid = q.record().indexOf("routeurid");
    QList<Parametre> l = {};
    while (q.next())
    {
        qDebug() << q.value(id).toInt() << q.value(parefeu).toString() << q.value(filtrage).toString() <<  q.value(control).toString() <<  q.value(routeurid).toInt();
        Parametre p(q.value(id).toInt(), q.value(parefeu).toString(), q.value(filtrage).toString(),  q.value(control).toString(),  q.value(routeurid).toInt());
        l.append(p);
    }
    return l;
}

QList<Parametre> Parametre::getAllByRouteur(int routeur_id) const
{
    QSqlQuery q;
    q.prepare("SELECT * FROM parametres where routeurid=:id;");
    q.bindValue(":id", routeur_id);
    QList<Parametre> l = {};
    if(q.exec())
    {
        int id = q.record().indexOf("id");
        int parefeu = q.record().indexOf("parefeu");
        int filtrage = q.record().indexOf("filtrage");
        int control = q.record().indexOf("control");
        int routeurid = q.record().indexOf("routeurid");
        while (q.next())
        {
            Parametre route(q.value(id).toInt(), q.value(parefeu).toString(), q.value(filtrage).toString(),  q.value(control).toString(),  q.value(routeurid).toInt());
            l.append(route);
        }
    }
    return l;
}

bool Parametre::save() const
{
    bool success = false;
    if (!parefeu.isEmpty() && !filtrage.isEmpty() && !control.isEmpty() && routeurid!=0)
    {
        QSqlQuery q;
        q.prepare("INSERT INTO parametres (parefeu, filtrage, control, routeurid) VALUES (:parefeu, :filtrage, :control, :routeurid);");
        q.bindValue(":id", this->id);
        q.bindValue(":parefeu", this->parefeu);
        q.bindValue(":filtrage", this->filtrage);
        q.bindValue(":control", this->control);
        q.bindValue(":routeurid", this->routeurid);
        if(q.exec())
        {
            success = true;
            QMessageBox::about(0, QObject::tr("Success"), "Parametre enregistré avec succès.");
        }
        else
        {
            QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de la route"), q.lastError().text());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de la route"), "Veuillez remplir tous les champs svp");
    }

    return success;
}

bool Parametre::update() const
{
    bool success = false;

    if (!parefeu.isEmpty() && !filtrage.isEmpty() && !control.isEmpty() && routeurid!=0)
    {
        QSqlQuery q;
        q.prepare("UPDATE parametres SET parefeu = :parefeu, filtrage = :filtrage, control = :control WHERE id=:id;");
        q.bindValue(":id", this->id);
        q.bindValue(":control", this->control);
        q.bindValue(":filtrage", this->filtrage);
        q.bindValue(":parefeu", this->parefeu);

        if(q.exec())
        {
            success = true;
            QMessageBox::about(0, QObject::tr("Success"), "Parametre mis a jour avec succès.");
        }
        else
        {
            QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de la route"), q.lastError().text());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de la route"), "Veuillez remplir tous les champs svp");
    }

    return success;
}


bool Parametre::remove()
{
    bool success = false;
    QSqlQuery q;
    q.prepare("DELETE FROM parametres WHERE id = :id");
    q.bindValue(":id", this->getId());
    if(q.exec())
    {
        success = true;
        QMessageBox::about(0, QObject::tr("Success"), "Parametre supprimé avec succès.");
    }
    else {
        QMessageBox::critical(0, QObject::tr("Echec lors de la suppression de la route"), q.lastError().text());
    }

    return success;
}

bool Parametre::removeAllBelongToRouteur(int& routeurid)
{
    bool success = false;

    QSqlQuery q;
    q.prepare("DELETE FROM parametres");
    if (q.exec())
    {
        success = true;
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec suppression des parametres de ce routeur"), q.lastError().text());
    }

    return success;
}


int Parametre::getId(){
    return this->id;
}

QString Parametre::getParefeu(){
    return this->parefeu;
}

QString Parametre::getFiltrage(){
    return this->filtrage;
}

QString Parametre::getControl(){
    return this->control;
}

int Parametre::getRouteurid(){
    return this->routeurid;
}

//Routeur Parametre::getRouteurid(){
//    Routeurp = p.getOne(this->getRouteurid());
//    returnp;
//}

void Parametre::setId(int id) {
    this->id = id;
}

void Parametre::setParefeu(QString parefeu){
    this->parefeu = parefeu;
}

void Parametre::setFiltrage(QString filtrage){
    this->filtrage = filtrage;
}

void Parametre::setControl(QString control){
    this->control = control;
}

void Parametre::setRouteurid(int routeurid){
    this->routeurid = routeurid;
}
