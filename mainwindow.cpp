#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QRegularExpression>
#include <QStyleFactory>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    reloadTable();
    connect(ui->exitAction, &QAction::triggered, qApp, &QApplication::quit);

    QRegularExpression regExp("^.(.*)\\+?Style$");
    QString defaultStyle = QApplication::style()->metaObject()->className();
    QRegularExpressionMatch match = regExp.match(defaultStyle);

    if (match.hasMatch())
        defaultStyle = match.captured(1);


    QFile file(":/qss/coffee.qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QString::fromLatin1(file.readAll());

    qApp->setStyleSheet(styleSheet);

}

void MainWindow::on_aboutAction_triggered()
{
    QMessageBox::about(this, tr("About Style sheet"),
        tr("<h1>Application du TP C++</h1>"
           "<p>Ce tp représente une application qui se comporte comme un simulateur de réseaux. Notre programme permet la création d'un routeur et de ses interfaces. </p>"
           "<p>Il permet de fixer les adresses IP du réseau, de configurer les routes du routeur. Une route est constituée de l’adresse du réseau, son masque et l’adresse de la passerelle.  Avec notre programme on peut également configurer des paramètres de sécurité tels que les pare feux, les filtrage web et les contrôles d’accès. Grâce a une interface on peut informer si il est possible d'accéder a une autre machine du réseau a partir de notre routeur.</p>"
           "<p>L'interface graphique de notre application à été réalisée grâce à Qt par : <ul><li>Ismène DEGUENONVO</li> <li>Jospy GOUDALO</li></ul> </p>"));
}
void MainWindow::on_actualiserRouteurList_triggered()
{
    reloadTable();
}


void MainWindow::reloadTable()
{
    ui->routeurTableWidget->setRowCount(0);
    Routeur r;
    QList<Routeur> l = r.getAll();
    int taille = l.size();

    for (int row=0; row<taille; row++ )
    {
        int idint = l[row].getId();
        QString idr = QString::number(idint);
        ui->routeurTableWidget->insertRow(ui->routeurTableWidget->rowCount());
        ui->routeurTableWidget->setItem(ui->routeurTableWidget->rowCount()-1,0, new QTableWidgetItem(idr));
        ui->routeurTableWidget->setItem(ui->routeurTableWidget->rowCount()-1,1, new QTableWidgetItem(l[row].getNumero()));
        ui->routeurTableWidget->setItem(ui->routeurTableWidget->rowCount()-1,2, new QTableWidgetItem(l[row].getIp()));
    }
}

void MainWindow::on_ajouterRouteurBtn_clicked()
{
    AddRouteurDialog a;
    a.exec();
}


void MainWindow::on_routeurTableWidget_cellDoubleClicked(int row, int column)
{

    QModelIndex index = ui->routeurTableWidget->selectionModel()->selectedIndexes()[0];
    int id=index.sibling(index.row(), 0).data().toInt();
    DetailsRouteurDialog d(id);
    d.exec();
}


//// overloading event(QEvent*) method of QMainWindow
//// l' overloading des events me permet de mettre a jour le table a chaque fois quelle est active
//bool MainWindow::event(QEvent* e)
//{
//    switch (e->type())
//    {
//    case QEvent::WindowActivate:
//        reloadTable();
//    break;
//    };
//    return QMainWindow::event(e);
//}

//bool YourWidget::event(QEvent *e)
//{
//    if (e->type() == QEvent::WindowActivate) {
//        // window was activated
//    }
//    return QWidget::event(e);
//}

//void MyWidget::changeEvent(QEvent * e) {
//    if(e->type() == QEvent::ActivationChange && this->isActiveWindow()) {
//       // .. this is now the active window
//    }
//}
