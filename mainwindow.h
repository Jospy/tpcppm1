#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableView>
#include <QStandardItemModel>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QMessageBox>
#include <QDebug>

#include "detailsrouteurdialog.h"
#include "addrouteurdialog.h"
#include "dbmanager.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; class ajouterRouteurView; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

    //~MainWindow();
    void reloadTable();

    // bool event(QEvent* e);

private slots:
    void on_ajouterRouteurBtn_clicked();
    void on_aboutAction_triggered();
    void on_actualiserRouteurList_triggered();

    void on_routeurTableWidget_cellDoubleClicked(int row, int column);

private:
    Ui::MainWindow *ui;


};



#endif // MAINWINDOW_H
