#include "detailsroutedialog.h"
#include "ui_detailsroutedialog.h"

DetailsRouteDialog::DetailsRouteDialog(int id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DetailsRouteDialog)
{
    ui->setupUi(this);
    Route r = r.getOne(id);
    this->routeid = r.getId();
    ui->ipInput->setText(r.getIp());
    ui->maskInput->setText(r.getMask());
    ui->passerelleInput->setText(r.getPasserelle());
}

DetailsRouteDialog::~DetailsRouteDialog()
{
    delete ui;
}

void DetailsRouteDialog::on_enregistrerRouteBtn_clicked()
{
    QString ip = ui->ipInput->text();
    QString mask = ui->maskInput->text();
    QString passerelle = ui->passerelleInput->text();

    Route r = r.getOne(this->routeid);
    r.setIp(ip);
    r.setMask(mask);
    r.setPasserelle(passerelle);
    if(r.update())
    {
        this->close();
    }
}

void DetailsRouteDialog::on_removeBtn_clicked()
{
    QMessageBox::StandardButton resBtn;
    resBtn = QMessageBox::question( this, "Suppression de la route",tr("Etes vous sur de vouloir supprimer cette route?"),
    QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
    QMessageBox::Yes);
    if (resBtn == QMessageBox::Yes)
    {
        Route r = r.getOne(this->routeid);
        if(r.remove()){
          this->close();
        };
    }
}

void DetailsRouteDialog::on_cancelBtn_clicked()
{
    this->close();
}
