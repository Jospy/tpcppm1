#ifndef ADDINTERFACEDIALOG_H
#define ADDINTERFACEDIALOG_H

#include <QDialog>
#include "interface.h"
namespace Ui {
class AddInterfaceDialog;
}

class AddInterfaceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddInterfaceDialog(int routeurid, QWidget *parent = nullptr);
    ~AddInterfaceDialog();

private slots:
    void on_enregistrerBtn_clicked();

    void on_cancelBtn_2_clicked();

private:
    Ui::AddInterfaceDialog *ui;
    int routeurid;
};

#endif // ADDINTERFACEDIALOG_H
