#include <QApplication>
#include "mainwindow.h"
#include <QSql>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QDebug>
#include <dbmanager.h>
#include <routeur.h>
#include <route.h>

static const QString path = "example.db";

int main(int argc, char *argv[])

{
    QApplication a(argc, argv);
    DbManager db(path);

    if (!db.isOpen())
    {
        qDebug() << "Erreur de connexion a la base de données.";
        return 1;
    }
    db.createTable();

    MainWindow w;
    w.show();

    return a.exec();
}
