#include "addparametredialog.h"
#include "ui_addparametredialog.h"

AddParametreDialog::AddParametreDialog(int routeurid, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddParametreDialog)
{
    this->routeurid = routeurid;
    ui->setupUi(this);
}

AddParametreDialog::~AddParametreDialog()
{
    delete ui;
}

void AddParametreDialog::on_cancelBtn_clicked()
{
    this->close();
}


void AddParametreDialog::on_enregistrerBtn_clicked()
{
    QString parefeu = ui->parefeuInput->text();
    QString filtrage = ui->filtrageInput->text();
    QString control = ui->controlInput->text();

    qDebug() << parefeu << filtrage << control << this->routeurid;
    Parametre r(parefeu, filtrage, control, this->routeurid);
    if(r.save()){
      this->close();
    };
}
