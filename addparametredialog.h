#ifndef ADDPARAMETREDIALOG_H
#define ADDPARAMETREDIALOG_H

#include <QDialog>
#include "parametre.h"
namespace Ui {
class AddParametreDialog;
}

class AddParametreDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddParametreDialog(int routeurid, QWidget *parent = nullptr);
    ~AddParametreDialog();

private slots:
    void on_cancelBtn_clicked();

    void on_enregistrerBtn_clicked();

private:
    Ui::AddParametreDialog *ui;
    int routeurid;
};

#endif // ADDPARAMETREDIALOG_H
