QT       += core gui widgets
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
requires(qtConfig(combobox))

CONFIG += c++11

# REALISE PAR :
# Ismène DEGUENONVO
# Jospy GOUDALO
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addinterfacedialog.cpp \
    addparametredialog.cpp \
    addroutedialog.cpp \
    addrouteurdialog.cpp \
    dbmanager.cpp \
    detailsinterfacedialog.cpp \
    detailsparametredialog.cpp \
    detailsroutedialog.cpp \
    detailsrouteurdialog.cpp \
    interface.cpp \
    main.cpp \
    mainwindow.cpp \
    parametre.cpp \
    route.cpp \
    routeur.cpp

HEADERS += \
    addinterfacedialog.h \
    addparametredialog.h \
    addroutedialog.h \
    addrouteurdialog.h \
    dbmanager.h \
    detailsinterfacedialog.h \
    detailsparametredialog.h \
    detailsroutedialog.h \
    detailsrouteurdialog.h \
    interface.h \
    mainwindow.h \
    parametre.h \
    route.h \
    routeur.h


RESOURCES += \
    stylesheet.qrc


FORMS += \
    addinterfacedialog.ui \
    addparametredialog.ui \
    addroutedialog.ui \
    addrouteurdialog.ui \
    detailsinterfacedialog.ui \
    detailsparametredialog.ui \
    detailsroutedialog.ui \
    detailsrouteurdialog.ui \
    mainwindow.ui

TRANSLATIONS += \
    tp_fr_BJ.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

