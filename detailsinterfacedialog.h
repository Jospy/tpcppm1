#ifndef DETAILSINTERFACEDIALOG_H
#define DETAILSINTERFACEDIALOG_H

#include <QDialog>
#include "interface.h"

namespace Ui {
class DetailsInterfaceDialog;
}

class DetailsInterfaceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DetailsInterfaceDialog(int id, QWidget *parent = nullptr);
    ~DetailsInterfaceDialog();

private slots:
    void on_enregistrerBtn_clicked();

    void on_removeBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::DetailsInterfaceDialog *ui;
    int interfaceid;
};

#endif // DETAILSINTERFACEDIALOG_H
