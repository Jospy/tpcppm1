#include "detailsinterfacedialog.h"
#include "ui_detailsinterfacedialog.h"

DetailsInterfaceDialog::DetailsInterfaceDialog(int id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DetailsInterfaceDialog)
{
    ui->setupUi(this);
    Interface interface = interface.getOne(id);
    this->interfaceid = interface.getId();
    ui->ipInput->setText(interface.getIp());
    ui->nomInput->setText(interface.getNom());
    //ui->connectionInput->setChecked(interface.getConnection() ? true : false);
}

DetailsInterfaceDialog::~DetailsInterfaceDialog()
{
    delete ui;
}

void DetailsInterfaceDialog::on_removeBtn_clicked()
{
    QMessageBox::StandardButton resBtn;
    resBtn = QMessageBox::question( this, "Suppression de la interface",tr("Etes vous sur de vouloir supprimer cette interface?"),
    QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
    QMessageBox::Yes);
    if (resBtn == QMessageBox::Yes)
    {
        Interface interface = interface.getOne(this->interfaceid);
        if(interface.remove()){
          this->close();
        };
    }
}

void DetailsInterfaceDialog::on_cancelBtn_clicked()
{
    this->close();
}

void DetailsInterfaceDialog::on_enregistrerBtn_clicked()
{
    QString ip = ui->ipInput->text();
    QString nom = ui->nomInput->text();
    //int connection = ui->connectionInput->isChecked() ? 1 : 0;

    Interface interface = interface.getOne(this->interfaceid);
    interface.setIp(ip);
    interface.setNom(nom);
    //interface.setConnection(connection);
    if(interface.update())
    {
        this->close();
    }
}
