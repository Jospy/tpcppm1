#include "routeur.h"

Routeur::Routeur() {
    this->id = 0;
    this->numero = "";
    this->ip = "";
}

Routeur::Routeur(int id, QString numero, QString ip) {
    this->id = id;
    this->numero = numero;
    this->ip = ip;
}

Routeur::Routeur(QString numero, QString ip) {
    this->id = 0;
    this->numero = numero;
    this->ip = ip;
}


Routeur Routeur::getOne(const int& id) const
{
    Routeur r;
    QSqlQuery q;
    q.prepare("SELECT * FROM routeurs WHERE id = (:id)");
    q.bindValue(":id", id);
    if (q.exec())
    {
        int id = q.record().indexOf("id");
        int ip = q.record().indexOf("ip");
        int numero = q.record().indexOf("numero");
        if (q.next())
        {
            r.setId(q.value(id).toInt());
            r.setNumero(q.value(numero).toString());
            r.setIp(q.value(ip).toString());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec: Routeur introuvable"), q.lastError().text());
    }
    return r;
}

QList<Routeur> Routeur::getAll() const
{
    QSqlQuery q("SELECT * FROM routeurs");
    int id = q.record().indexOf("id");
    int ip = q.record().indexOf("ip");
    int numero = q.record().indexOf("numero");
    QList<Routeur> l = {};
    while (q.next())
    {
        Routeur r(q.value(id).toInt(), q.value(numero).toString(), q.value(ip).toString());
        l.append(r);
    }
    return l;
}


bool Routeur::save() const
{
    bool success = false;
    if (!numero.isEmpty() && !ip.isEmpty())
    {
        QSqlQuery q;
        q.prepare("INSERT INTO routeurs (numero, ip) VALUES (:numero, :ip);");
        q.bindValue(":numero", this->numero);
        q.bindValue(":ip", this->ip);
        if(q.exec())
        {
            success = true;
            QMessageBox::about(0, QObject::tr("Success"), "Routeur enregistré avec succès.");
        }
        else
        {
            QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout routeur"), q.lastError().text());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout routeur"), "Veuillez remplir tous les champs svp");
    }
    return success;
}

bool Routeur::update() const
{
    bool success = false;
    if (!numero.isEmpty() && !ip.isEmpty())
    {
        QSqlQuery q;
        q.prepare("UPDATE routeurs SET numero = :numero, ip = :ip WHERE id=:id;");
        q.bindValue(":id", this->id);
        q.bindValue(":numero", this->numero);
        q.bindValue(":ip", this->ip);
        if(q.exec())
        {
            success = true;
            QMessageBox::about(0, QObject::tr("Success"), "Routeur mis a jour avec succès.");
        }
        else
        {
            QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout routeur"), q.lastError().text());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout routeur"), "Veuillez remplir tous les champs svp");
    }
    return success;
}


bool Routeur::remove()
{
    bool success = false;
    QSqlQuery q;
    q.prepare("DELETE FROM routeurs WHERE id = :id");
    q.bindValue(":id", this->getId());
    if(q.exec())
    {
        success = true;
        QMessageBox::about(0, QObject::tr("Success"), "Routeur supprimé avec succès.");
    }
    else {
        QMessageBox::critical(0, QObject::tr("Echec lors de la suppression du routeur"), q.lastError().text());
    }
    return success;
}

bool Routeur::removeAll()
{
    bool success = false;
    QSqlQuery q;
    q.prepare("DELETE FROM routeurs");
    if (q.exec())
    {
        success = true;
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec suppression des routeurs"), q.lastError().text());
    }
    return success;
}


int Routeur::getId() const {
    return this->id;
}

QString Routeur::getNumero(){
    return this->numero;
}

QString Routeur::getIp(){
    return this->ip;
}

void Routeur::setId(int id) {
    this->id = id;
}

void Routeur::setNumero(QString numero) {
    this->numero = numero;
}

void Routeur::setIp(QString ip) {
    this->ip = ip;
}


