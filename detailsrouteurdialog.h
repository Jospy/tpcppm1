#ifndef DETAILSROUTEURDIALOG_H
#define DETAILSROUTEURDIALOG_H

#include <QDialog>
#include <QDebug>
#include <QTableWidget>
#include <QMessageBox>
#include "routeur.h"
#include "route.h"
#include "addroutedialog.h"
#include "addparametredialog.h"
#include "addinterfacedialog.h"

class Routeur;
class Route;

namespace Ui {
class DetailsRouteurDialog;
}

class DetailsRouteurDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DetailsRouteurDialog(int id, QWidget *parent = nullptr);
    ~DetailsRouteurDialog();

    void reloadTable();
    void reloadRouteTable();
    void reloadInterfaceTable();
    void reloadParametreTable();
    void loadView(int);

private slots:
    void on_closeDetails_clicked();
    void on_deleteRouteur_clicked();
    void on_enregistrerInput_clicked();

    void on_addRouteBtn_clicked();

    void on_addParametreBtn_clicked();

    void on_addInterfaceBtn_clicked();

    void on_routeTableWidget_cellDoubleClicked(int row, int column);


    void on_reloadRoute_clicked();

    void on_reloadParametre_clicked();

    void on_reloadInterface_clicked();

    void on_interfaceTableWidget_cellDoubleClicked(int row, int column);

    void on_parametreTableWidget_cellDoubleClicked(int row, int column);

private:
    Ui::DetailsRouteurDialog *ui;
    int routeur_id;

};

#endif // DETAILSROUTEURDIALOG_H
