#ifndef DETAILSROUTEDIALOG_H
#define DETAILSROUTEDIALOG_H

#include <QDialog>
#include "route.h"

namespace Ui {
class DetailsRouteDialog;
}

class DetailsRouteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DetailsRouteDialog(int id, QWidget *parent = nullptr);
    ~DetailsRouteDialog();

private slots:
    void on_enregistrerRouteBtn_clicked();

    void on_removeBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::DetailsRouteDialog *ui;
    int routeid;
};

#endif // DETAILSROUTEDIALOG_H
