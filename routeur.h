#ifndef ROUTEUR_H
#define ROUTEUR_H

#include "dbmanager.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>
#include <QMessageBox>
#include <QSqlDatabase>
#include "route.h"


class Routeur
{
public:
   // constructeurs
   Routeur();
   Routeur(int, QString, QString);
   Routeur(QString, QString);

   //QList<Routeur> getAll();

   // getters
   int getId() const;
   QString getIp();
   QString getNumero();

   // setters
   void setId(int);
   void setNumero(QString);
   void setIp(QString);

   // interactions avec la base de données
   // en admetant que nous disposons de l'objet:
   bool save() const; // enregistrer l'objet dans la base
   bool update() const; // modifier l'objet dans la base

   bool remove(); // le supprimer
   bool removeAll(); //supprimer tous les objet (pas disponible au niveau de la vue)

   QList<Routeur> getAll() const; // recuperer la liste des router
   Routeur getOne(const int& id) const;

//   QList<Routeur> getAll() const; // recuperer la liste des router
//   QList<Routeur> getAll() const; // recuperer la liste des router


private:
   QSqlDatabase m_db;

   int id;
   QString numero;
   QString ip;

};

#endif // ROUTEUR_H
