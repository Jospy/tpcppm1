#ifndef DETAILSPARAMETREDIALOG_H
#define DETAILSPARAMETREDIALOG_H

#include <QDialog>
#include "parametre.h"

namespace Ui {
class DetailsParametreDialog;
}

class DetailsParametreDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DetailsParametreDialog(int id, QWidget *parent = nullptr);
    ~DetailsParametreDialog();

private slots:
    void on_removeBtn_clicked();

    void on_cancelBtn_clicked();

    void on_enregistrerBtn_clicked();

private:
    Ui::DetailsParametreDialog *ui;
    int parametreid;
};

#endif // DETAILSPARAMETREDIALOG_H
