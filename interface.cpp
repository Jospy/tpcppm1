#include "interface.h"


Interface::Interface(){
    this->id = 0;
    this->ip = "";
    this->nom = "";
    this->connection = false;
    this->routeurid = 0;
}

Interface::Interface(int id, QString ip, QString nom, int connection, int routeurid ){
    this->id = id;
    this->ip = ip;
    this->nom = nom;
    this->connection = connection;
    this->routeurid = routeurid;
}

Interface::Interface(QString ip, QString nom, int connection, int routeurid ){
    this->id = 0;
    this->ip = ip;
    this->nom = nom;
    this->connection = connection;
    this->routeurid = routeurid;
}

Interface Interface::getOne(const int& id) const
{
    Interface interface;
    QSqlQuery q;
    q.prepare("SELECT * FROM interfaces WHERE id = (:id)");
    q.bindValue(":id", id);
    if (q.exec())
    {
        int id = q.record().indexOf("id");
        int ip = q.record().indexOf("ip");
        int nom = q.record().indexOf("nom");
        int connection = q.record().indexOf("connection");
        int routeurid = q.record().indexOf("routeurid");
        if (q.next())
        {
            interface.setId(q.value(id).toInt());
            interface.setIp(q.value(ip).toString());
            interface.setNom(q.value(nom).toString());
            interface.setConnection(q.value(connection).toInt());
            interface.setRouteurid(q.value(routeurid).toInt());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec: Interface introuvable"), q.lastError().text());
    }
    return interface;
}

QList<Interface> Interface::getAll() const
{
    QSqlQuery q("SELECT * FROM interfaces");
    int id = q.record().indexOf("id");
    int ip = q.record().indexOf("ip");
    int nom = q.record().indexOf("nom");
    int connection = q.record().indexOf("connection");
    int routeurid = q.record().indexOf("routeurid");
    QList<Interface> l = {};
    while (q.next())
    {
        Interface interface(q.value(id).toInt(), q.value(ip).toString(), q.value(nom).toString(),  q.value(connection).toInt(),  q.value(routeurid).toInt());
        l.append(interface);
    }
    return l;
}

QList<Interface> Interface::getAllByRouteur(int routeur_id) const
{
    QSqlQuery q;
    q.prepare("SELECT * FROM interfaces where routeurid=:id;");
    q.bindValue(":id", routeur_id);
    QList<Interface> l = {};
    if(q.exec())
    {
        int id = q.record().indexOf("id");
        int ip = q.record().indexOf("ip");
        int nom = q.record().indexOf("nom");
        int connection = q.record().indexOf("connection");
        int routeurid = q.record().indexOf("routeurid");
        while (q.next())
        {
            Interface interface(q.value(id).toInt(), q.value(ip).toString(), q.value(nom).toString(),  q.value(connection).toInt(),  q.value(routeurid).toInt());
            l.append(interface);
        }
    }
    return l;
}

bool Interface::save() const
{
    bool success = false;
    if (!ip.isEmpty() && !nom.isEmpty() && routeurid!=0)
    {
        QSqlQuery q;
        q.prepare("INSERT INTO interfaces (ip, nom, connection, routeurid) VALUES (:ip, :nom, :connection, :routeurid);");
        q.bindValue(":id", this->id);
        q.bindValue(":ip", this->ip);
        q.bindValue(":nom", this->nom);
        q.bindValue(":connection", this->connection);
        q.bindValue(":routeurid", this->routeurid);
        if(q.exec())
        {
            success = true;
            QMessageBox::about(0, QObject::tr("Success"), "Interface enregistré avec succès.");
        }
        else
        {
            QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de l'interface"), q.lastError().text());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de l'interface"), "Veuillez remplir tous les champs svp");
    }

    return success;
}

bool Interface::update() const
{
    bool success = false;

    if (!ip.isEmpty() && !nom.isEmpty() && routeurid!=0)
    {
        qDebug() << this->connection;

        QSqlQuery q;
        q.prepare("UPDATE interfaces SET ip = :ip, nom = :nom WHERE id=:id;");
        q.bindValue(":id", this->id);
//        q.bindValue(":connection", this->connection);
        q.bindValue(":nom", this->nom);
        q.bindValue(":ip", this->ip);

        if(q.exec())
        {
            success = true;
            QMessageBox::about(0, QObject::tr("Success"), "Interface mis a jour avec succès.");
        }
        else
        {
            QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de l'interface"), q.lastError().text());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de l'interface"), "Veuillez remplir tous les champs svp");
    }

    return success;
}


bool Interface::remove()
{
    bool success = false;
    QSqlQuery q;
    q.prepare("DELETE FROM interfaces WHERE id = :id");
    q.bindValue(":id", this->getId());
    if(q.exec())
    {
        success = true;
        QMessageBox::about(0, QObject::tr("Success"), "Interface supprimé avec succès.");
    }
    else {
        QMessageBox::critical(0, QObject::tr("Echec lors de la suppression de l'interface"), q.lastError().text());
    }

    return success;
}

bool Interface::removeAllBelongToRouteur(int& routeurid)
{
    bool success = false;

    QSqlQuery q;
    q.prepare("DELETE FROM interfaces");
    if (q.exec())
    {
        success = true;
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec suppression des interfaces de ce routeur"), q.lastError().text());
    }

    return success;
}


int Interface::getId(){
    return this->id;
}

QString Interface::getIp(){
    return this->ip;
}

QString Interface::getNom(){
    return this->nom;
}

int Interface::getConnection(){
    return this->connection;
}

int Interface::getRouteurid(){
    return this->routeurid;
}

//Routeur Interface::getRouteurid(){
//    Routeur interface = interface.getOne(this->getRouteurid());
//    return interface;
//}

void Interface::setId(int id) {
    this->id = id;
}

void Interface::setIp(QString ip){
    this->ip = ip;
}

void Interface::setNom(QString nom){
    this->nom = nom;
}

void Interface::setConnection(int connection){
    this->connection = connection;
}

void Interface::setRouteurid(int routeurid){
    this->routeurid = routeurid;
}


