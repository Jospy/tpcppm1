#include "route.h"


Route::Route(){
    this->id = 0;
    this->ip = "";
    this->mask = "";
    this->passerelle = "";
    this->routeurid = 0;
}

Route::Route(int id, QString ip, QString mask, QString passerelle, int routeurid ){
    this->id = id;
    this->ip = ip;
    this->mask = mask;
    this->passerelle = passerelle;
    this->routeurid = routeurid;
}

Route::Route(QString ip, QString mask, QString passerelle, int routeurid ){
    this->id = 0;
    this->ip = ip;
    this->mask = mask;
    this->passerelle = passerelle;
    this->routeurid = routeurid;
}

Route Route::getOne(const int& id) const
{
    Route r;
    QSqlQuery q;
    q.prepare("SELECT * FROM routes WHERE id = (:id)");
    q.bindValue(":id", id);
    if (q.exec())
    {
        int id = q.record().indexOf("id");
        int ip = q.record().indexOf("ip");
        int mask = q.record().indexOf("mask");
        int passerelle = q.record().indexOf("passerelle");
        int routeurid = q.record().indexOf("routeurid");
        if (q.next())
        {
            r.setId(q.value(id).toInt());
            r.setIp(q.value(ip).toString());
            r.setMask(q.value(mask).toString());
            r.setPasserelle(q.value(passerelle).toString());
            r.setRouteurid(q.value(routeurid).toInt());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec: Route introuvable"), q.lastError().text());
    }
    return r;
}

QList<Route> Route::getAll() const
{
    QSqlQuery q("SELECT * FROM routes");
    int id = q.record().indexOf("id");
    int ip = q.record().indexOf("ip");
    int mask = q.record().indexOf("mask");
    int passerelle = q.record().indexOf("passerelle");
    int routeurid = q.record().indexOf("routeurid");
    QList<Route> l = {};
    while (q.next())
    {
        Route r(q.value(id).toInt(), q.value(ip).toString(), q.value(mask).toString(),  q.value(passerelle).toString(),  q.value(routeurid).toInt());
        l.append(r);
    }
    return l;
}

QList<Route> Route::getAllByRouteur(int routeur_id) const
{
    QSqlQuery q;
    q.prepare("SELECT * FROM routes where routeurid=:id;");
    q.bindValue(":id", routeur_id);
    QList<Route> l = {};
    if(q.exec())
    {
        int id = q.record().indexOf("id");
        int ip = q.record().indexOf("ip");
        int mask = q.record().indexOf("mask");
        int passerelle = q.record().indexOf("passerelle");
        int routeurid = q.record().indexOf("routeurid");
        while (q.next())
        {
            Route route(q.value(id).toInt(), q.value(ip).toString(), q.value(mask).toString(),  q.value(passerelle).toString(),  q.value(routeurid).toInt());
            l.append(route);
        }
    }
    return l;
}

bool Route::save() const
{
    bool success = false;
    if (!ip.isEmpty() && !mask.isEmpty() && !passerelle.isEmpty() && routeurid!=0)
    {
        QSqlQuery q;
        q.prepare("INSERT INTO routes (ip, mask, passerelle, routeurid) VALUES (:ip, :mask, :passerelle, :routeurid);");
        q.bindValue(":id", this->id);
        q.bindValue(":ip", this->ip);
        q.bindValue(":mask", this->mask);
        q.bindValue(":passerelle", this->passerelle);
        q.bindValue(":routeurid", this->routeurid);
        if(q.exec())
        {
            success = true;
            QMessageBox::about(0, QObject::tr("Success"), "Route enregistré avec succès.");
        }
        else
        {
            QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de la route"), q.lastError().text());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de la route"), "Veuillez remplir tous les champs svp");
    }

    return success;
}

bool Route::update() const
{
    bool success = false;

    if (!ip.isEmpty() && !mask.isEmpty() && !passerelle.isEmpty() && routeurid!=0)
    {
        QSqlQuery q;
        q.prepare("UPDATE routes SET ip = :ip, mask = :mask, passerelle = :passerelle WHERE id=:id;");
        q.bindValue(":id", this->id);
        q.bindValue(":passerelle", this->passerelle);
        q.bindValue(":mask", this->mask);
        q.bindValue(":ip", this->ip);

        if(q.exec())
        {
            success = true;
            QMessageBox::about(0, QObject::tr("Success"), "Route mis a jour avec succès.");
        }
        else
        {
            QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de la route"), q.lastError().text());
        }
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec lors de l'ajout de la route"), "Veuillez remplir tous les champs svp");
    }

    return success;
}


bool Route::remove()
{
    bool success = false;
    QSqlQuery q;
    q.prepare("DELETE FROM routes WHERE id = :id");
    q.bindValue(":id", this->getId());
    if(q.exec())
    {
        success = true;
        QMessageBox::about(0, QObject::tr("Success"), "Route supprimé avec succès.");
    }
    else {
        QMessageBox::critical(0, QObject::tr("Echec lors de la suppression de la route"), q.lastError().text());
    }

    return success;
}

bool Route::removeAllBelongToRouteur(int& routeurid)
{
    bool success = false;

    QSqlQuery q;
    q.prepare("DELETE FROM routes");
    if (q.exec())
    {
        success = true;
    }
    else
    {
        QMessageBox::critical(0, QObject::tr("Echec suppression des routes de ce routeur"), q.lastError().text());
    }

    return success;
}


int Route::getId(){
    return this->id;
}

QString Route::getIp(){
    return this->ip;
}

QString Route::getMask(){
    return this->mask;
}

QString Route::getPasserelle(){
    return this->passerelle;
}

int Route::getRouteurid(){
    return this->routeurid;
}

//Routeur Route::getRouteurid(){
//    Routeur r = r.getOne(this->getRouteurid());
//    return r;
//}

void Route::setId(int id) {
    this->id = id;
}

void Route::setIp(QString ip){
    this->ip = ip;
}

void Route::setMask(QString mask){
    this->mask = mask;
}

void Route::setPasserelle(QString passerelle){
    this->passerelle = passerelle;
}

void Route::setRouteurid(int routeurid){
    this->routeurid = routeurid;
}


